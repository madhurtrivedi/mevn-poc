A simple POC application developed in MEVN(MongoDB, Express, VueJs, NodeJs) stack for demonstration of Database connection, API development and CRUD operations.

Dependencies to run this application: 1. MongoDB installed on the local machine.

To run the application: 
1. npm install to install dependencies. 
2. Start MongoDB locally with "mongod" command. 
3. Start the local server by cd into the directory "API" and apply command "nodemon server". 
4. Run the application in browser by cd into the project directory and apply command "npm serve".

